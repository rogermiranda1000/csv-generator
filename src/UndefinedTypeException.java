public class UndefinedTypeException extends RuntimeException {
    public UndefinedTypeException(String err) { super(err); }
}
