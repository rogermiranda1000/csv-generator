import java.util.ArrayList;

public class Generator {
    public static String generate(int row, Type...types) {
        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < row; x++) {
            for (Type type : types) {
                sb.append(type.get());
                sb.append(';');
            }
            sb.setLength(sb.length()-1); // remove last ';'
            sb.append('\n');
        }

        return sb.toString();
    }

    public static <T> String generate(ArrayList<T> values, Type...types) {
        StringBuilder sb = new StringBuilder();

        for (T val : values) {
            sb.append(val.toString());
            sb.append(';');

            for (Type type : types) {
                sb.append(type.get());
                sb.append(';');
            }

            sb.setLength(sb.length()-1); // remove last ';'
            sb.append('\n');
        }

        return sb.toString();
    }
}
