import com.github.javafaker.Faker;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Random;

public enum Type {
    ADDRESS,
    DATE,
    NAME,
    FIRST_NAME,
    LAST_NAME,
    EMAIL,
    PRODUCT,
    SMALL_INTEGER,
    BIG_INTEGER,
    /**
     * Lorem ipsum
     */
    LOREM;

    private static final Faker faker = new Faker(new Locale("es"));
    private static final Random random = new Random();
    private static final int SMALL_VALUES = 256;
    private static final int BIG_VALUES = 300000;

    public String get() throws UndefinedTypeException {
        switch (this) {
            case ADDRESS:
                return Type.faker.address().streetAddress();
            case DATE:
                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                return formatter.format(Type.faker.date().birthday());
            case NAME:
                return Type.faker.name().fullName();
            case FIRST_NAME:
                return Type.faker.name().firstName();
            case LAST_NAME:
                return Type.faker.name().lastName();
            case EMAIL:
                return Type.faker.internet().emailAddress();
            case PRODUCT:
                return Type.faker.commerce().productName();
            case LOREM:
                return Type.faker.lorem().sentence();
            case SMALL_INTEGER:
                return String.valueOf(random.nextInt(SMALL_VALUES));
            case BIG_INTEGER:
                return String.valueOf(random.nextInt(BIG_VALUES));
            default:
                throw new UndefinedTypeException("Couldn't get '" + this.name() + "'.");
        }
    }
}
