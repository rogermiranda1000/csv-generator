import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    public static void main(String []args) {
        try {
            FileWriter file = new FileWriter("files/out.csv");

            file.write("address;date;name;price\n");

            file.write(Generator.generate(5, Type.ADDRESS, Type.DATE, Type.NAME, Type.SMALL_INTEGER));
            file.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            System.err.flush();
            ex.printStackTrace();
        }

        try {
            FileWriter file = new FileWriter("files/out.csv");
            ArrayList<Integer> codes = new ArrayList<>();
            for (int x = 1; x <= 1000; x++) codes.add(x);

            file.write("code;name;size;weight;creation_date;cost\n");

            file.write(Generator.generate(codes, Type.PRODUCT, Type.SMALL_INTEGER, Type.SMALL_INTEGER, Type.DATE, Type.SMALL_INTEGER));
            file.close();
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
            System.err.flush();
            ex.printStackTrace();
        }
    }
}
